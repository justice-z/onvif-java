package com.onvif;

import java.net.ConnectException;

import javax.xml.soap.SOAPException;

import com.onvif.soap.OnvifDevice;

public class Main {

	private static final String INFO = "Commands:\n  \n  url: Get snapshort URL.\n  info: Get information about each valid command.\n  profiles: Get all profiles.\n  exit: Exit this application.";

	public static void main(String args[]) {
		String cameraAddress = "192.168.0.208";
		String user = "admin";
		String password = "admin";
		OnvifDevice cam;
		try {
			cam = new OnvifDevice(cameraAddress, user, password);
		}
		catch (ConnectException | SOAPException e1) {
			System.err.println("No connection to camera, please try again.");
			return;
		}
		System.out.println("Connection to camera successful!");

	}
}